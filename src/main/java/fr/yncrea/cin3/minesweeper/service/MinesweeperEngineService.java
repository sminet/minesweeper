package fr.yncrea.cin3.minesweeper.service;

import fr.yncrea.cin3.minesweeper.domain.Cell;
import fr.yncrea.cin3.minesweeper.domain.Mine;
import fr.yncrea.cin3.minesweeper.domain.Minefield;
import fr.yncrea.cin3.minesweeper.domain.MinefieldStatus;
import fr.yncrea.cin3.minesweeper.exception.MinesweeperException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Stack;

@Service
public class MinesweeperEngineService {
    public Minefield create(long width, long height, long count) {
        if (width < 0 || height < 0) {
            throw new MinesweeperException("Invalid minefield size");
        }

        if (count < 0 || count > (width * height)) {
            throw new MinesweeperException("Invalid number of mines");
        }

        Minefield minefield = new Minefield(width, height);
        minefield.setCount(count);

        // create a list of all possible mines
        LinkedList<Mine> mines = new LinkedList<>();
        for (long x = 0; x < width; ++x) {
            for (long y = 0; y < height; ++y) {
                mines.add(new Mine(x, y));
            }
        }

        // shuffle the list
        Collections.shuffle(mines);

        // then pick the last "count" required
        for (long cpt = 0; cpt < count; ++cpt) {
            Mine mine = mines.pop();
            this.addMine(minefield, mine.getX(), mine.getY());
        }

        return minefield;
    }

    public void play(Minefield minefield, long x, long y) {
        // check if minefield is already finished
        if (minefield.getStatus() != MinefieldStatus.STARTED) {
            throw new MinesweeperException("Game has already ended");
        }

        // check range validity
        if (!inRange(minefield, x, y)) {
            throw new MinesweeperException("Invalid position for a new mine");
        }

        // create a new cell
        Cell cell = new Cell(x, y, getMineCountNear(minefield, x, y));

        // check if already discovered
        if (minefield.getCells().contains(cell)) {
            throw new MinesweeperException("That cell was already discovered");
        }

        // then discover the cell
        if (hasMine(minefield, x, y)) {
            gameLost(minefield);
            return;
        }

        // and add it to the discovered cell collection
        minefield.addCell(cell);

        // if cell is empty discover around
        if (cell.getNear() == 0L) {
            floodFill(minefield, cell);
        }

        // check if game a ended
        long availableCells = minefield.getWidth() * minefield.getHeight() - minefield.getCount();
        if (minefield.getCells().size() == availableCells) {
            gameWon(minefield);
        }
    }

    public void addMine(Minefield minefield, long x, long y) {
        // check range validity
        if (!inRange(minefield, x, y)) {
            throw new MinesweeperException("Invalid position for a new mine");
        }

        Mine mine = new Mine(x, y);
        minefield.addMine(mine);
    }

    public long getMineCountNear(Minefield minefield, long x, long y) {
        long count = 0;
        for (Mine mine: minefield.getMines()) {
            if (mine.getX() >= x - 1 && mine.getX() <= x + 1 && mine.getY() >= y - 1 && mine.getY() <= y + 1) {
                count++;
            }
        }

        return count;
    }

    public boolean hasMine(Minefield minefield, long x, long y) {
        Mine mine = new Mine(x, y);
        return minefield.getMines().contains(mine);
    }

    public boolean isDiscovered(Minefield minefield, long x, long y) {
        Cell cell = new Cell(x, y, 0);
        return minefield.getCells().contains(cell);
    }

    private boolean inRange(Minefield minefield, long x, long y) {
        if (x < 0 || x >= minefield.getWidth()) {
            return false;
        }

        if (y < 0 || y >= minefield.getHeight()) {
            return false;
        }

        return true;
    }

    private void gameLost(Minefield minefield) {
        minefield.setStatus(MinefieldStatus.LOST);

        // discover all existing mines (since it is a set, add already added cells)
        for (Mine mine: minefield.getMines()) {
            long x = mine.getX();
            long y = mine.getY();

            minefield.addCell(new Cell(x, y, getMineCountNear(minefield, x, y)));
        }
    }

    private void gameWon(Minefield minefield) {
        minefield.setStatus(MinefieldStatus.WIN);
    }

    private void floodFill(Minefield minefield, Cell cell) {
        Stack<Cell> stack = new Stack<>();
        stack.add(cell);

        while (stack.empty() == false) {
            Cell object = stack.pop();

            // discover cell
            minefield.addCell(object);

            if (object.getNear() == 0) {
                long x = object.getX();
                long y = object.getY();

                for (int vx = -1; vx <= 1; ++vx) {
                    for (int vy = -1; vy <= 1; ++vy) {
                        long newX = x + vx;
                        long newY = y + vy;

                        if ((vx == 0 && vy == 0) || inRange(minefield, newX, newY) == false) {
                            continue;
                        }

                        // if not already discovered, add to the list
                        if (isDiscovered(minefield, newX, newY) == false) {
                            // create a new cell
                            stack.add(new Cell(newX, newY, getMineCountNear(minefield, newX, newY)));
                        }
                    }
                }
            }
        }
    }
}
