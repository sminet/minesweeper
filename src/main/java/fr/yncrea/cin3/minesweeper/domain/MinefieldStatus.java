package fr.yncrea.cin3.minesweeper.domain;

public enum MinefieldStatus {
    STARTED, WIN, LOST
}
