package fr.yncrea.cin3.minesweeper.domain;

import lombok.*;

import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Mine {
    private long x;
    private long y;
}
