package fr.yncrea.cin3.minesweeper.domain;

import lombok.*;

import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode(exclude = {"near"})
@AllArgsConstructor
@NoArgsConstructor
public class Cell {
    private long x;
    private long y;

    /**
     * number of mines around that cell
     */
    private long near;
}
