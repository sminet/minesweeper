package fr.yncrea.cin3.minesweeper.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
public class Minefield {
    @Id
    @GeneratedValue
    private UUID id;

    private long width;
    private long height;
    private long count;

    @Enumerated(EnumType.STRING)
    private MinefieldStatus status = MinefieldStatus.STARTED;

    @ElementCollection
    private Set<Mine> mines = new HashSet<>();

    @ElementCollection
    private Set<Cell> cells = new HashSet<>();

    public Minefield() {}

    public Minefield(long width, long height) {
        this.width = width;
        this.height = height;
    }

    public void addMine(Mine mine) {
        mines.add(mine);
    }

    public void addCell(Cell cell) {
        cells.add(cell);
    }
}
