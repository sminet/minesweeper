package fr.yncrea.cin3.minesweeper.controller;

import fr.yncrea.cin3.minesweeper.domain.Minefield;
import fr.yncrea.cin3.minesweeper.exception.MinesweeperException;
import fr.yncrea.cin3.minesweeper.form.MinefieldForm;
import fr.yncrea.cin3.minesweeper.repository.MinefieldRepository;
import fr.yncrea.cin3.minesweeper.service.MinesweeperEngineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("/minesweeper")
public class MinesweeperController {
    @Autowired
    private MinesweeperEngineService engine;

    @Autowired
    private MinefieldRepository minefields;

    @GetMapping("/create")
    public String create(Model model) {
        MinefieldForm form = new MinefieldForm();
        form.setWidth(20L);
        form.setHeight(10L);
        form.setCount(15L);

        model.addAttribute("form", form);

        return "minesweeper/create";
    }

    @PostMapping("/create")
    public String createPost(@Valid @ModelAttribute("form") MinefieldForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("form", form);
            return "minesweeper/create";
        }

        try {
            Minefield minefield = engine.create(form.getWidth(), form.getHeight(), form.getCount());
            minefields.save(minefield);

            return "redirect:/minesweeper/play/" + minefield.getId();
        } catch (MinesweeperException e) {
            model.addAttribute("form", form);
            model.addAttribute("error", e.getMessage());
            return "minesweeper/create";
        }
    }

    @GetMapping({"/play/{id}", "/play/{id}/{x}/{y}"})
    public String play(@PathVariable UUID id, @PathVariable(required = false) Long x, @PathVariable(required = false) Long y, Model model) {
        // get the minefield
        var minefield = minefields.findById(id).orElseThrow(() -> new RuntimeException("Not found"));

        // if we need to discover a new cell
        if (x != null && y != null) {
            try {
                engine.play(minefield, x, y);
                minefields.save(minefield);
            } catch (MinesweeperException e) {
                model.addAttribute("message", e.getMessage());
            }
        }

        // transform the minefield in a array for display
        String[][] fieldDto = new String[(int)minefield.getHeight()][(int)minefield.getWidth()];
        for (int cx = 0; cx < minefield.getWidth(); ++cx) {
            for (int cy = 0; cy < minefield.getHeight(); ++cy) {
                if (engine.isDiscovered(minefield, cx, cy) == false) {
                    // cells not discovered yet, put null
                    fieldDto[cy][cx] = null;
                } else if (engine.hasMine(minefield, cx, cy)) {
                    // cell discovered, and contains an exploded mine
                    fieldDto[cy][cx] = "mine";
                } else {
                    // cell discovered, without mine, display number of mines around
                    fieldDto[cy][cx] = String.format("%d", engine.getMineCountNear(minefield, cx, cy));
                }
            }
        }

        model.addAttribute("minefield", minefield);
        model.addAttribute("field", fieldDto);

        return "minesweeper/play";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable UUID id) {
        minefields.deleteById(id);

        return "redirect:/";
    }
}
