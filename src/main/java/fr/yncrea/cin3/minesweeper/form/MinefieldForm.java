package fr.yncrea.cin3.minesweeper.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MinefieldForm {
    @NotNull
    @Min(0)
    @Max(20)
    private Long width;

    @NotNull
    @Min(0)
    @Max(20)
    private Long height;

    @NotNull
    @Min(0)
    private Long count;
}
